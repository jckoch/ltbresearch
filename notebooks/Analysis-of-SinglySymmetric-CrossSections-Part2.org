#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t broken-links:nil c:nil creator:nil
#+OPTIONS: d:(not "LOGBOOK") date:t e:t email:nil f:t inline:t num:t
#+OPTIONS: p:nil pri:nil prop:nil stat:t tags:t tasks:t tex:t
#+OPTIONS: timestamp:t title:t toc:t todo:t |:t
#+TITLE: Analysis of Singly-Symmetric Cross-Sections (Part 2)
#+DATE: <2018-07-25 Wed>
#+AUTHOR: James Koch
#+EMAIL: jckoch@ualberta.ca
#+LANGUAGE: en
#+SELECT_TAGS: export
#+EXCLUDE_TAGS: noexport
#+CREATOR: Emacs 25.3.1 (Org mode 9.1.13)

* Evaluation of Effect of Critical Elastic Buckling Moment due to Errors in the Asymmetry Parameter

*** Set Up Python Environment
:PROPERTIES:
:CUSTOM_ID: set-up-python-environment
:END:

Import relevant python packages.

#+BEGIN_SRC ipython :async
  import os
  import time
  import pandas as pd
  import numpy as np
  import matplotlib.pyplot as plt
  import matplotlib.ticker as ticker
#+END_SRC

#+RESULTS:
:RESULTS:
:END:

Set up figure style as per the Canadian Journal of Civil Engineering
specifications.

#+BEGIN_SRC ipython :async
  %matplotlib inline
  # Matplotlib Default Parameters
  plt.rcParams['legend.fontsize'] = 'medium'
#+END_SRC

#+RESULTS:
:RESULTS:
:END:

*** Input of Pre-Calculated Data from the Part 1

#+BEGIN_SRC ipython :async
  dfallnew = pd.read_csv('./../data/madeupsinglysymmetric_properties.csv')
#+END_SRC

#+RESULTS:
:RESULTS:
:END:

*** Calculate Elastic Section Modulus

#+BEGIN_SRC ipython :async
  dfS = pd.DataFrame()
  dfS['Sxc'] = dfallnew['Ix'] / (dfallnew['y_bar_1'] + dfallnew['T'] / 2)
  dfS['Sxt'] = dfallnew['Ix'] / (dfallnew['y_bar_2'] + dfallnew['T'] / 2)
  dfallnew["Sx"] = dfS[["Sxc", "Sxt"]].min(axis=1)
  dfS['Syc'] = dfallnew['Iy'] / (dfallnew['y_bar_1'] + dfallnew['T'] / 2)
  dfS['Syt'] = dfallnew['Iy'] / (dfallnew['y_bar_2'] + dfallnew['T'] / 2)
  dfallnew["Sy"] = dfS[["Syc", "Syt"]].min(axis=1)
#+END_SRC

#+RESULTS:
:RESULTS:
:END:

*** Define Relevant Material Properties
:PROPERTIES:
:CUSTOM_ID: define-relevant-material-properties
:END:

#+BEGIN_SRC ipython :async
  Fy = 350 # MPa
  sqrtFy = np.sqrt(Fy)
  E = 200000 # MPa
  G = 77000 # MPa
#+END_SRC

#+RESULTS:
:RESULTS:
:END:

*** Specify a Practical Beam Length

#+BEGIN_SRC ipython :var sL = 1 fL = 60 step = 1 :async
  L = np.arange(sL, fL + step, step) * 10**3  # mm
#+END_SRC

#+RESULTS:
:RESULTS:
:END:

*** Critical Elastic Moment Calculation

#+BEGIN_SRC ipython :async
  def calc_moment(txt):
      # initialize temporary dataframes
      df1 = pd.DataFrame()
      df2 = pd.DataFrame()
      df3 = pd.DataFrame()
      df4 = pd.DataFrame()
      # initialize Mcr numpy array
      Mcr = np.zeros([len(dfallnew), len(L)])
      Kbar = np.zeros([len(dfallnew), len(L)])
      Ycr = np.zeros([len(dfallnew), len(L)])
      # calculate elastic - inelastic LTB boundary
      dfallnew['M'] = 0.7 * dfallnew['Sx'] * Fy
      # Compute Mcr
      for i in range(len(L)):
          # Calculate required parameters
          Mcr[:, i] = ((np.pi**2 * E * dfallnew['Iy']) / (2 * L[i]**2)) * (dfallnew[txt] + np.sqrt(dfallnew[txt]**2 + 4 * ((G * dfallnew['J'] * L[i]**2) / (np.pi**2 * E * dfallnew['Iy']) + dfallnew['Cw'] / dfallnew['Iy'])))
          Kbar[:, i] = np.sqrt((np.pi**2 * E * dfallnew['Iy'] * dfallnew['d_prime']**2) / (4 * G * dfallnew['J'] * L[i]**2))
          Ycr[:, i] = (Mcr[:, i] * L[i]) / np.sqrt(E * dfallnew['Iy'] * G * dfallnew['J'])
          # Populate new dataframes
          str1 = 'Mcr_%d_mm' % L[i]
          df1[str1] = Mcr[:, i]
          str2 = 'LTBType_%d_mm' % L[i]
          df2[str2] = np.where(df1[str1] <= dfallnew['M'], 'elastic', 'inelastic')
          str3 = 'Kbar_%d_mm' % L[i]
          df3[str3] = Kbar[:, i]
          str4 = 'Ycr_%d_mm' % L[i]
          df4[str4] = Ycr[:, i]

      # Create final columns in main dataframe
      dfallnew['Mcr_' + txt[5:]] = df1.apply(lambda r: tuple(r), axis=1).apply(np.array)
      dfallnew['LTB Type'] = df2.apply(lambda r: tuple(r), axis=1).apply(np.array)
      dfallnew['Kbar'] = df3.apply(lambda r: tuple(r), axis=1).apply(np.array)
      dfallnew['Ycr_%d_mm'] = df4.apply(lambda r: tuple(r), axis=1).apply(np.array)
      return(df1, df2, df3, df4)

  Mcr_calc, LTBType_calc, Kbar, Ycr_calc = calc_moment('BetX_calc')  
  # print('%d | %d | %d | %d | %d |' % (len(dfallnew), len(Mcr_calc), len(LTBType_calc), len(Kbar), len(Ycr_calc)))
  Mcr_KnT, LTBType_KnT, Kbar_KnT, Ycr_KnT = calc_moment('BetX_K&T')
  Mcr_S16, LTBType_S16, Kbar_S16, Ycr_S16 = calc_moment('BetX_S16-14')
#+END_SRC

#+RESULTS:
:RESULTS:
:END:

*** Critical Elastic Moment Error resulting from use of approximate \(\beta_x\) formula

#+BEGIN_SRC ipython :async
  error_KnT = ((Mcr_KnT - Mcr_calc) / np.absolute(Mcr_calc)) * 100
  error_S16 = ((Mcr_S16 - Mcr_calc) / np.absolute(Mcr_calc)) * 100
#+END_SRC

#+RESULTS:
:RESULTS:
:END:

*** Limit Cross-Sections to Class 3 or greater

#+BEGIN_SRC ipython :display text/plain
  tf = dfallnew['Section Class'] <= 3
  dfxsec = dfallnew[tf]
  print('%d were removed due to Class 4 behaviour!!!' % (len(dfallnew) - len(dfxsec)))
  print('%d sections remain!!!' % len(dfxsec))

  dfmcr_calc = Mcr_calc[tf]
  dfLTBType_calc = LTBType_calc[tf]
  Kbar = Kbar[tf]
  Ycr_calc = Ycr_calc[tf]
  print('%d sections remain!!!' % len(dfmcr_calc))

  dfmcr_KnT = Mcr_KnT[tf]
  dfLTBType_KnT = LTBType_KnT[tf]
  Kbar_KnT = Kbar_KnT[tf]
  Ycr_KnT = Ycr_KnT[tf]
  error_KnT = error_KnT[tf]
  print('%d sections remain!!!' % len(dfmcr_KnT))

  dfmcr_S16 = Mcr_S16[tf]
  dfLTBType_S16 = LTBType_S16[tf]
  Kbar_S16 = Kbar_S16[tf]
  Ycr_S16 = Ycr_S16[tf]
  error_S16 = error_S16[tf]
  print('%d sections remain!!!' % len(dfmcr_S16))
#+END_SRC

#+RESULTS:
:RESULTS:
# output
: 294 were removed due to Class 4 behaviour!!!
: 16018 sections remain!!!
: 16018 sections remain!!!
: 16018 sections remain!!!
: 16018 sections remain!!!
: 
:END:

*** Statistics of Type of LTB

#+BEGIN_SRC ipython :async :display text/org
  print('|-------+-----------------------------+------+-----------+------+------+------+-----------|------+------|')
  print('| L (m) | No. of Elastic LTB Sections |  K&T |           |      |      |  S16 |           |      |      |')
  print('|       |                             | Mean | Std. Dev. | Max. | Min. | Mean | Std. Dev. | Max. | Min. |')
  print('|-------+-----------------------------+------+-----------+------+------+------+-----------|------+------|')
  for i in range(len(L)):
      str1 = 'LTBType_%d_mm' % L[i]
      str2 = 'Mcr_%d_mm' % L[i]
      tf1 = (dfLTBType_KnT[str1] == 'elastic')
      tf2 = (dfLTBType_S16[str1] == 'elastic')
      count1 = error_KnT[str2][tf1].count()  # .describe()
      mean1 = error_KnT[str2][tf1].mean()
      std1 = error_KnT[str2][tf1].std()
      max1 = error_KnT[str2][tf1].max()
      min1 = error_KnT[str2][tf1].min()
      count2 = error_S16[str2][tf2].count()  # .describe()
      mean2 = error_S16[str2][tf2].mean()
      std2 = error_S16[str2][tf2].std()
      max2 = error_S16[str2][tf2].max()
      min2 = error_S16[str2][tf2].min()
      print('| %.1f  | %d (%d)                     | %.2f | %.2f      | %.2f | %.2f | %.2f | %.2f      | %.2f | %.2f |' % (L[i]/1000, count1, len(dfxsec) - count1, mean1, std1, max1, min1, mean2, std2, max2, min2))
#+END_SRC

#+RESULTS:
:RESULTS:
# output
|-------+-----------------------------+-------+-----------+------+-------+-------+-----------+------+-------|
| L (m) | No. of Elastic LTB Sections |   K&T |           |      |       |   S16 |           |      |       |
|       |                             |  Mean | Std. Dev. | Max. |  Min. |  Mean | Std. Dev. | Max. |  Min. |
|-------+-----------------------------+-------+-----------+------+-------+-------+-----------+------+-------|
|   1.0 | 505 (15513)                 | -3.69 |      2.67 | 2.75 | -9.90 | -3.70 |      2.67 | 2.67 | -9.90 |
|   2.0 | 954 (15064)                 | -1.69 |      3.03 | 3.04 | -9.65 | -1.71 |      3.02 | 2.88 | -9.65 |
|   3.0 | 1476 (14542)                | -0.62 |      2.84 | 2.97 | -9.41 | -0.66 |      2.83 | 2.87 | -9.41 |
|   4.0 | 2075 (13943)                | -0.07 |      2.54 | 4.70 | -9.11 | -0.13 |      2.52 | 4.06 | -9.11 |
|   5.0 | 2710 (13308)                |  0.17 |      2.27 | 4.96 | -8.75 |  0.10 |      2.26 | 4.64 | -8.75 |
|   6.0 | 3423 (12595)                |  0.26 |      2.03 | 4.89 | -8.37 |  0.17 |      2.03 | 4.33 | -8.37 |
|   7.0 | 4190 (11828)                |  0.28 |      1.84 | 4.42 | -7.98 |  0.19 |      1.85 | 3.91 | -7.98 |
|   8.0 | 4962 (11056)                |  0.26 |      1.68 | 4.51 | -7.59 |  0.16 |      1.70 | 4.37 | -7.59 |
|   9.0 | 5733 (10285)                |  0.23 |      1.57 | 5.69 | -7.22 |  0.13 |      1.60 | 5.23 | -7.22 |
|  10.0 | 6483 (9535)                 |  0.21 |      1.47 | 5.35 | -6.86 |  0.10 |      1.50 | 4.92 | -6.86 |
|  11.0 | 7202 (8816)                 |  0.19 |      1.38 | 5.05 | -6.52 |  0.08 |      1.41 | 4.64 | -6.52 |
|  12.0 | 7901 (8117)                 |  0.16 |      1.29 | 4.77 | -6.19 |  0.05 |      1.33 | 4.38 | -6.19 |
|  13.0 | 8580 (7438)                 |  0.13 |      1.22 | 4.54 | -5.90 |  0.02 |      1.25 | 4.14 | -5.90 |
|  14.0 | 9207 (6811)                 |  0.09 |      1.16 | 4.31 | -5.62 | -0.01 |      1.19 | 3.92 | -5.62 |
|  15.0 | 9775 (6243)                 |  0.07 |      1.10 | 4.10 | -5.36 | -0.04 |      1.13 | 4.02 | -5.36 |
|  16.0 | 10283 (5735)                |  0.05 |      1.04 | 3.91 | -5.12 | -0.06 |      1.07 | 3.80 | -5.12 |
|  17.0 | 10749 (5269)                |  0.03 |      0.99 | 4.25 | -4.89 | -0.07 |      1.02 | 4.25 | -4.89 |
|  18.0 | 11170 (4848)                |  0.03 |      0.95 | 4.94 | -4.69 | -0.07 |      0.98 | 4.76 | -4.69 |
|  19.0 | 11524 (4494)                |  0.02 |      0.91 | 5.00 | -4.49 | -0.07 |      0.94 | 4.66 | -4.49 |
|  20.0 | 11839 (4179)                |  0.02 |      0.88 | 5.23 | -4.31 | -0.07 |      0.90 | 4.95 | -4.31 |
|  21.0 | 12141 (3877)                |  0.02 |      0.85 | 5.05 | -4.15 | -0.07 |      0.87 | 4.78 | -4.15 |
|  22.0 | 12382 (3636)                |  0.02 |      0.82 | 5.07 | -3.99 | -0.06 |      0.84 | 5.07 | -3.99 |
|  23.0 | 12613 (3405)                |  0.03 |      0.80 | 4.86 | -3.85 | -0.06 |      0.82 | 4.86 | -3.85 |
|  24.0 | 12806 (3212)                |  0.03 |      0.78 | 4.66 | -3.71 | -0.05 |      0.80 | 4.66 | -3.71 |
|  25.0 | 12987 (3031)                |  0.03 |      0.76 | 4.83 | -3.58 | -0.05 |      0.77 | 4.83 | -3.58 |
|  26.0 | 13158 (2860)                |  0.03 |      0.74 | 5.59 | -3.46 | -0.05 |      0.75 | 5.59 | -3.46 |
|  27.0 | 13299 (2719)                |  0.03 |      0.71 | 5.38 | -3.35 | -0.05 |      0.73 | 5.38 | -3.35 |
|  28.0 | 13441 (2577)                |  0.03 |      0.69 | 5.19 | -3.25 | -0.04 |      0.71 | 5.19 | -3.25 |
|  29.0 | 13570 (2448)                |  0.03 |      0.67 | 5.01 | -3.15 | -0.04 |      0.69 | 5.01 | -3.15 |
|  30.0 | 13678 (2340)                |  0.03 |      0.66 | 6.55 | -3.86 | -0.04 |      0.67 | 6.55 | -3.87 |
|  31.0 | 13779 (2239)                |  0.03 |      0.64 | 6.34 | -3.74 | -0.04 |      0.65 | 6.34 | -3.75 |
|  32.0 | 13887 (2131)                |  0.03 |      0.63 | 6.14 | -3.63 | -0.04 |      0.64 | 6.14 | -3.64 |
|  33.0 | 13952 (2066)                |  0.03 |      0.61 | 5.95 | -3.52 | -0.04 |      0.62 | 5.95 | -3.53 |
|  34.0 | 14038 (1980)                |  0.03 |      0.60 | 5.78 | -4.10 | -0.04 |      0.61 | 5.78 | -4.12 |
|  35.0 | 14126 (1892)                |  0.03 |      0.58 | 5.61 | -3.99 | -0.03 |      0.59 | 5.61 | -4.00 |
|  36.0 | 14183 (1835)                |  0.02 |      0.57 | 5.45 | -3.89 | -0.03 |      0.58 | 5.45 | -3.90 |
|  37.0 | 14238 (1780)                |  0.02 |      0.56 | 5.30 | -3.78 | -0.03 |      0.56 | 5.30 | -3.80 |
|  38.0 | 14313 (1705)                |  0.02 |      0.54 | 5.16 | -4.91 | -0.03 |      0.55 | 5.16 | -4.92 |
|  39.0 | 14369 (1649)                |  0.02 |      0.53 | 5.03 | -4.79 | -0.03 |      0.54 | 5.03 | -4.80 |
|  40.0 | 14412 (1606)                |  0.02 |      0.52 | 4.90 | -5.12 | -0.03 |      0.53 | 4.90 | -5.13 |
|  41.0 | 14479 (1539)                |  0.02 |      0.51 | 4.78 | -5.00 | -0.03 |      0.52 | 4.78 | -5.01 |
|  42.0 | 14533 (1485)                |  0.02 |      0.50 | 4.67 | -4.89 | -0.03 |      0.51 | 4.67 | -4.90 |
|  43.0 | 14570 (1448)                |  0.02 |      0.49 | 4.56 | -4.78 | -0.03 |      0.50 | 4.56 | -4.79 |
|  44.0 | 14620 (1398)                |  0.02 |      0.48 | 5.25 | -4.67 | -0.03 |      0.49 | 5.25 | -4.68 |
|  45.0 | 14663 (1355)                |  0.02 |      0.47 | 5.14 | -4.57 | -0.03 |      0.48 | 5.14 | -4.58 |
|  46.0 | 14719 (1299)                |  0.02 |      0.47 | 5.02 | -4.48 | -0.03 |      0.47 | 5.02 | -4.48 |
|  47.0 | 14753 (1265)                |  0.02 |      0.46 | 4.91 | -4.38 | -0.03 |      0.46 | 4.91 | -4.39 |
|  48.0 | 14799 (1219)                |  0.02 |      0.45 | 4.81 | -4.30 | -0.03 |      0.46 | 4.81 | -4.30 |
|  49.0 | 14836 (1182)                |  0.01 |      0.44 | 4.71 | -4.50 | -0.03 |      0.45 | 4.71 | -4.50 |
|  50.0 | 14863 (1155)                |  0.01 |      0.43 | 4.61 | -4.41 | -0.03 |      0.44 | 4.61 | -4.42 |
|  51.0 | 14902 (1116)                |  0.01 |      0.43 | 4.52 | -4.33 | -0.03 |      0.43 | 4.52 | -4.33 |
|  52.0 | 14939 (1079)                |  0.01 |      0.42 | 4.43 | -4.24 | -0.03 |      0.43 | 4.43 | -4.25 |
|  53.0 | 14964 (1054)                |  0.01 |      0.42 | 4.35 | -4.17 | -0.03 |      0.42 | 4.35 | -4.17 |
|  54.0 | 14999 (1019)                |  0.01 |      0.41 | 4.27 | -4.09 | -0.03 |      0.41 | 4.27 | -4.10 |
|  55.0 | 15037 (981)                 |  0.01 |      0.40 | 4.19 | -4.02 | -0.03 |      0.41 | 4.19 | -4.02 |
|  56.0 | 15066 (952)                 |  0.01 |      0.40 | 4.11 | -3.95 | -0.03 |      0.40 | 4.11 | -3.95 |
|  57.0 | 15085 (933)                 |  0.01 |      0.39 | 4.04 | -3.88 | -0.02 |      0.40 | 4.04 | -3.89 |
|  58.0 | 15105 (913)                 |  0.01 |      0.39 | 3.97 | -3.82 | -0.02 |      0.39 | 3.97 | -3.82 |
|  59.0 | 15132 (886)                 |  0.01 |      0.38 | 3.90 | -3.75 | -0.02 |      0.38 | 3.90 | -3.76 |
|  60.0 | 15162 (856)                 |  0.01 |      0.37 | 3.83 | -3.69 | -0.02 |      0.38 | 3.83 | -3.70 |

:END:

*** Statistics of Elastic LTB for 3 different lengths

#+BEGIN_SRC ipython
  error_KnT_1000 = error_KnT['Mcr_1000_mm'][(dfLTBType_KnT['LTBType_1000_mm'] == 'elastic')]
  error_KnT_5000 = error_KnT['Mcr_5000_mm'][(dfLTBType_KnT['LTBType_5000_mm'] == 'elastic')]
  error_KnT_20000 = error_KnT['Mcr_20000_mm'][(dfLTBType_KnT['LTBType_20000_mm'] == 'elastic')]
  error_KnT_60000 = error_KnT['Mcr_60000_mm'][(dfLTBType_KnT['LTBType_60000_mm'] == 'elastic')]

  error_S16_1000 = error_S16['Mcr_1000_mm'][(dfLTBType_S16['LTBType_1000_mm'] == 'elastic')]
  error_S16_5000 = error_S16['Mcr_5000_mm'][(dfLTBType_S16['LTBType_5000_mm'] == 'elastic')]
  error_S16_20000 = error_S16['Mcr_20000_mm'][(dfLTBType_S16['LTBType_20000_mm'] == 'elastic')]
  error_S16_60000 = error_S16['Mcr_60000_mm'][(dfLTBType_S16['LTBType_60000_mm'] == 'elastic')]
#+END_SRC

#+RESULTS:
:RESULTS:
:END:

#+BEGIN_SRC ipython
  def sephist(df):
      LHS = df[dfxsec[r'$\rho$'] <= 0.1]
      CTR = df[(dfxsec[r'$\rho$'] > 0.1) & (dfxsec[r'$\rho$'] < 0.9)]
      RHS = df[dfxsec[r'$\rho$'] >= 0.9]
      return(LHS, CTR, RHS)
#+END_SRC

#+RESULTS:
:RESULTS:
:END:

**** K&T and S16 Approximation Results for \(L = 1\SI{m}, 5\SI{m}, 20\SI{m}, 60\SI{m}\)

#+BEGIN_SRC ipython :ipyfile ./figs/elastic_error_4lengths.png :display image/png
  fig2, ax2 = plt.subplots(nrows=4, ncols=2)
  ax2[0, 0].hist((sephist(error_KnT_1000)[0], sephist(error_KnT_1000)[1], sephist(error_KnT_1000)[2]), label=[r'$\rho \leq 0.1$', r'0.1 < $\rho < 0.9$', r'$\rho \geq 0.9$'], color=['r', 'b', 'g'])
  ax2[1, 0].hist((sephist(error_KnT_5000)[0], sephist(error_KnT_5000)[1], sephist(error_KnT_5000)[2]), label=[r'$\rho \leq 0.1$', r'0.1 < $\rho < 0.9$', r'$\rho \geq 0.9$'], color=['r', 'b', 'g'])
  ax2[2, 0].hist((sephist(error_KnT_20000)[0], sephist(error_KnT_20000)[1], sephist(error_KnT_20000)[2]), label=[r'$\rho \leq 0.1$', r'0.1 < $\rho < 0.9$', r'$\rho \geq 0.9$'], color=['r', 'b', 'g'])
  ax2[3, 0].hist((sephist(error_KnT_60000)[0], sephist(error_KnT_60000)[1], sephist(error_KnT_60000)[2]), label=[r'$\rho \leq 0.1$', r'0.1 < $\rho < 0.9$', r'$\rho \geq 0.9$'], color=['r', 'b', 'g'])
  ax2[0, 1].hist((sephist(error_S16_1000)[0], sephist(error_S16_1000)[1], sephist(error_S16_1000)[2]), label=[r'$\rho \leq 0.1$', r'0.1 < $\rho < 0.9$', r'$\rho \geq 0.9$'], color=['r', 'b', 'g'])
  ax2[1, 1].hist((sephist(error_S16_5000)[0], sephist(error_S16_5000)[1], sephist(error_S16_5000)[2]), label=[r'$\rho \leq 0.1$', r'0.1 < $\rho < 0.9$', r'$\rho \geq 0.9$'], color=['r', 'b', 'g'])
  ax2[2, 1].hist((sephist(error_S16_20000)[0], sephist(error_S16_20000)[1], sephist(error_S16_20000)[2]), label=[r'$\rho \leq 0.1$', r'0.1 < $\rho < 0.9$', r'$\rho \geq 0.9$'], color=['r', 'b', 'g'])
  ax2[3, 1].hist((sephist(error_S16_60000)[0], sephist(error_S16_60000)[1], sephist(error_S16_60000)[2]), label=[r'$\rho \leq 0.1$', r'0.1 < $\rho < 0.9$', r'$\rho \geq 0.9$'], color=['r', 'b', 'g'])
  plt.xlabel('.', color=(0, 0, 0, 0))
  plt.ylabel('.', color=(0, 0, 0, 0))
  plt.legend(bbox_to_anchor=(1.9, 4))
  plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
  fig2.text(0.5, 0.04, r'% Error of $M_{cr}$', va='center', ha='center', size='large')
  fig2.text(0.001, 0.5, 'Number of X-sections', va='center', ha='center', size='large', rotation='vertical')
#+END_SRC

#+RESULTS:
:RESULTS:
# image/png
[[file:./figs/elastic_error_4lengths.png]]
:END:

*** Plotting \(M_{cr}\) as a function of \(\bar{K}\)

#+BEGIN_SRC ipython :ipyfile ./figs/Ycr_Kbar_plot.png :display image/png
  rho = [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
  tol = [0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1]
  # tol = [0.0001, 0.003, 0.004, 0.004, 0.004, 0.02, 0.004, 0.004, 0.004, 0.003, 0.0]
  fig3, ax3 = plt.subplots(1, 1)
  for i in range(len(rho)):
      tfrho1 = (np.isclose(dfxsec[r'$\rho$'], rho[i], atol=tol[i])) 
      # print('%d and %d' % (len(Kbar[tfrho1]), len(Ycr_calc[tfrho1])))
      xdata = Kbar[tfrho1].values.ravel()
      ydata = Ycr_calc[tfrho1].values.ravel()
      poly = np.poly1d(np.polyfit(xdata, ydata, 2))
      x = np.linspace(0, 2.5, 25)
      labeltxt = r'$\rho = %.1f$' % rho[i]
      plt.plot(x, poly(x), label=labeltxt)
      plt.xlim(xmin=0, xmax=2.5)
      plt.ylim(ymin=0, ymax=25)
      plt.xlabel(r'$\bar{K}$', size='large')
      plt.ylabel(r'$\gamma_{cr}$', size='large')
      plt.legend(loc='center right', bbox_to_anchor=(1.3, 0.5))
      plt.tight_layout(pad=0.4, w_pad=0.5, h_pad=1.0)
      fig3.text(0.05, 0.225, r'$\pi$', size='large')
#+END_SRC

#+RESULTS:
:RESULTS:
# image/png
[[file:./figs/Ycr_Kbar_plot.png]]
:END:

#+BEGIN_SRC ipython  :ipyfile ./figs/Ycr_Kbar_plot.png :display image/png

#+END_SRC

