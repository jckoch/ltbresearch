* Evaluation of CSA S16-14 Asymmetry Parameter for Standard WT-shapes
  :PROPERTIES:
  :CUSTOM_ID: evaluation-of-csa-s16-14-asymmetry-parameter-for-standard-wt-shapes
  :END:

Standard WT-shapes are taken from CISC /Excel Steel Section Tables/
(version 9.2). (\beta\_x) will be calculated using a exact expression
and approximate formulas.

*** Table of Contents
    :PROPERTIES:
    :CUSTOM_ID: table-of-contents
    :END:

-  Set Up Python Environment
-  Data Input
-  Define Relevant Material Properties
-  Section Property Calculatons
-  Determine the Asymmetry Parameter, $\beta_x$
-  Determine % Difference of Calculating $\beta_x$ with and without
   including Fillet Areas
-  Determine % Error Between True and Approximate $\beta_x$
-  Write Data to a CSV file
-  Section Class Statistics
-  Percent Error Statistics
-  Plotting the % Error for All Standard WT-Shapes

*** Set Up Python Environment
    :PROPERTIES:
    :CUSTOM_ID: set-up-python-environment
    :END:

Import relevant python packages

#+BEGIN_SRC ipython :async
    import os
    import numpy as np
    import pandas as pd
    import matplotlib.pyplot as plt
    import seaborn as sns
#+END_SRC

Set up figure style as per the Canadian Journal of Civil Engineering
specifications.

#+BEGIN_SRC ipython :async
    %matplotlib inline
    def set_style():
        # This sets reasonable defaults for font size for
        # a figure that will go in a paper
        sns.set_context("paper")
        
        # Set the font to be serif, rather than sans
        sns.set(font='serif')
        
        # Make the background white, and specify the
        # specific font family
        sns.set_style("white", {
            "font.family": "serif",
            "font.serif": ["Times", "Palatino", "serif"]
        })
        
    # Single Column Plot (as per NRC CJCE specs)
    def set_size_onecol(fig):
        fig.set_size_inches(9.3, 3.4)
    # Two Column Plot (as per NRC CJCE specs)
    def set_size_twocol(fig):
        fig.set_size_inches(9.3, 7.2)

    # Matplotlib Default Parameters
    plt.rcParams['legend.fontsize'] = 'large'
#+END_SRC

*** Data Input
    :PROPERTIES:
    :CUSTOM_ID: data-input
    :END:

Load section data for *standard* W shapes. Data loaded include width and
thickness of compression and tension flanges, thickness of web, and
overall height of beam.

**** Notes on the Data
     :PROPERTIES:
     :CUSTOM_ID: notes-on-the-data
     :END:

-  It should be noted that the compression flange is assumed to be the
   top flange and the centriod of a cross-section is measured from the
   centriod of the top flange.
-  All section properties are reported in millimetres (mm).
-  Datum shall be top of the cross-section.

#+BEGIN_SRC ipython :async
    filename = 'c:/jk/ualberta/Courses/project_DRA/analysis/CISC_StructuralSectionTables/CISC_SST_ver92_WT-cleaned.csv'
    (path,file) = os.path.split(filename)
    dfall = pd.read_csv(filename)
#+END_SRC

*** Define Relevant Material Properties
    :PROPERTIES:
    :CUSTOM_ID: define-relevant-material-properties
    :END:

#+BEGIN_SRC ipython :async
    Fy = 350 # MPa
    sqrtFy = np.sqrt(Fy)
#+END_SRC

*** Section Property Calculatons
    :PROPERTIES:
    :CUSTOM_ID: section-property-calculatons
    :END:

Assuming that the datum is the centroid of the top flange.

**** Determine the distance between flange centroids.
     :PROPERTIES:
     :CUSTOM_ID: determine-the-distance-between-flange-centroids.
     :END:

#+BEGIN_SRC ipython :async
    dfall['d_prime'] = dfall['D'] - dfall['T']/2
#+END_SRC

**** Determine centroid of T-section.
     :PROPERTIES:
     :CUSTOM_ID: determine-centroid-of-t-section.
     :END:

#+BEGIN_SRC ipython :async
    dfall['yT_bar'] = dfall['T']/2 + dfall['Yo'] 
#+END_SRC

**** Determine major over minor moment of inertia ratio.
     :PROPERTIES:
     :CUSTOM_ID: determine-major-over-minor-moment-of-inertia-ratio.
     :END:

#+BEGIN_SRC ipython :async
    dfall['Iy/Ix'] = dfall['Iy']/dfall['Ix']
#+END_SRC

*** Section Classification
    :PROPERTIES:
    :CUSTOM_ID: section-classification
    :END:

Section classification based on flange and web local buckling checks.
Assuming a 350W Grade for steel, $F_y = 350MPa$.

-  Set up S6-14 code requirements for flanges

#+BEGIN_SRC ipython :async
    crit1 = 145
    crit2 = 170
    crit3 = 200
#+END_SRC

-  Determine section class

#+BEGIN_SRC ipython :async
    dfall['Flange Criteria'] = ((dfall['B']/2)/dfall['T'])*np.sqrt(Fy)
    dfall['Flange Class'] = np.where(dfall['Flange Criteria'] < crit1, 1,
                                     np.where(dfall['Flange Criteria'] < crit2, 2,
                                              np.where(dfall['Flange Criteria'] < crit3, 3, 4)))
    dfall['Section Class'] = dfall['Flange Class']
#+END_SRC

*** Determine the Asymmetry Parameter, $\beta_x$
    :PROPERTIES:
    :CUSTOM_ID: determine-the-asymmetry-parameter-beta_x
    :END:

Determine $\beta_x$ from true expression and from approximate formula
given in S16-14. Note that the true $\beta_x$ is calculated in two steps
where the flange contributions are determined first and then combined to
achieve the true value.

$\beta_{x}$ will be calculated based on the true integral expression and
various approximations.

The integral (true) expression for $\beta_{x}$ is calculated by:

$\beta_{x} = \frac{1}{I_{x}} (\int_{A}{x^{2}ydA} + \int_{A}{y^{3}dA}) - 2y_{o}$

Which can be further simplified to the following as per /Galambos
(1968)/.

$\beta_{x} = \frac{1}{I_x} ((h-\bar{y})((h-\bar{y})^3 \frac{w}{4}) - \bar{y}(\frac{b_c^3 t_t}{12} + b_c t_c \bar{y}^2 + \bar{y}^3 \frac{w}{4})) - 2y_o$

Approximation for $\beta_{x}$ is from /Kitipornchai & Trahair (1979)
Buckling Properties of Monosymmetric I-Beams/ and is calculated by:

$\beta_{x} = 0.9h(2\rho - 1)(1 - (\frac{I_y}{I_x})^{2})$, where
$\rho = \frac{I_{yc}}{I_{yc} + I_{yt}}$

Furthermore, $\beta_{x}$ is calculated according to the CSA S16-14
standard which is computed by:

$\beta_{x} = 0.9(d-t)(\frac{2I_{yc}}{I_{y}} - 1)(1 - (\frac{I_{y}}{I_{x}})^{2})$

#+BEGIN_SRC ipython :async
    dfall['term1'] = (1/dfall['Ix'])
    dfall['term2'] = (dfall['W']/4)*((dfall['D'] - dfall['yT_bar'])**4 - (dfall['yT_bar'] - dfall['T']/2)**4)
    dfall['term3'] = dfall['B']*dfall['T']*(dfall['yT_bar'] - dfall['T']/2)*(dfall['B']**2/12 +
                                                                             (dfall['yT_bar'] - dfall['T']/2)**2)
    dfall['term4'] = -2*dfall['Yo']
#+END_SRC

#+BEGIN_SRC ipython :async
    dfall['BetX_calc'] = (dfall['term2'] - dfall['term3'])/dfall['Ix'] - dfall['term4']
#+END_SRC

#+BEGIN_SRC ipython :async
    dfall['Iy_Flange1'] = ((dfall['B']**3)*dfall['T'])/12
    rho = 1.0
#+END_SRC

#+BEGIN_SRC ipython :async
    dfall['BetX_S16'] = (0.9*(dfall['D'] - dfall['T'])*((2*dfall['Iy_Flange1'])/dfall['Iy'] - 1) * 
                         (1 - (dfall['Iy']/dfall['Ix'])**2))
    dfall['BetX_K&T'] = (0.9*(dfall['D'] - dfall['T'])*(2*rho - 1)*(1 - (dfall['Iy']/dfall['Ix'])**2))
#+END_SRC

*** Determine % Difference of Calculating $\beta_x$ with and without
including Fillet Areas
    :PROPERTIES:
    :CUSTOM_ID: determine-difference-of-calculating-beta_x-with-and-without-including-fillet-areas
    :END:

#+BEGIN_SRC ipython :async
    dfall['% Difference with/without Including Fillets'] = (np.absolute(dfall['BetX_calc'] - dfall['BetX'])/dfall['BetX'])*100
#+END_SRC

#+BEGIN_SRC ipython :async
    plt.hist(dfall['% Difference with/without Including Fillets'])
    plt.xlabel('% Difference')
    plt.ylabel('Number of T-Sections')
    plt.show()
#+END_SRC

*** Determine % Error Between True and Approximate $\beta_x$
    :PROPERTIES:
    :CUSTOM_ID: determine-error-between-true-and-approximate-beta_x
    :END:

#+BEGIN_SRC ipython :async
    dfall['Percent Error S16-14'] = ((dfall['BetX_S16'] - (dfall['BetX']))/np.absolute(dfall['BetX']))*100
    dfall['Percent Error K&T'] = ((dfall['BetX_K&T'] - (dfall['BetX']))/np.absolute(dfall['BetX']))*100
#+END_SRC

*** Write Data to a CSV file
    :PROPERTIES:
    :CUSTOM_ID: write-data-to-a-csv-file
    :END:

#+BEGIN_SRC ipython :async
    dfall.to_csv('Handbook-Vs-Galambo_T-Section_BetX_Comparison.csv')
#+END_SRC

*** Section Class Statistics
    :PROPERTIES:
    :CUSTOM_ID: section-class-statistics
    :END:

#+BEGIN_SRC ipython :async
    print('The number of Class 1, 2, and 3 sections are: %d' % len(dfall[dfall['Section Class'] != 4])) 
    print('The number of Class 4 sections is: %d' % len(dfall[dfall['Section Class'] == 4]))
    print('Percentage of Class 4 Sections: %.2f' % 
          ((len(dfall[dfall['Section Class'] == 4])/len(dfall[dfall['Section Class'] != 4]))*100))
#+END_SRC

*** Percent Error Statistics
    :PROPERTIES:
    :CUSTOM_ID: percent-error-statistics
    :END:

#+BEGIN_SRC ipython :async
    print('Maximum Percent Error using K&T Approximation: %.3f' % max(dfall['Percent Error K&T']))
    print('Maximum Percent Error using S16-14 Approximation: %.3f' % max(dfall['Percent Error S16-14']))
    print('Minimum Percent Error using K&T Approximation: %.3f' % min(dfall['Percent Error K&T']))
    print('Minimum Percent Error using K&T Approximation: %.3f' % min(dfall['Percent Error S16-14']))
#+END_SRC

#+BEGIN_SRC ipython :async
    print('For sections with Iy/Ix < 0.5:')
    print('  + Minimum Percent Error using K&T Approximation: %.3f' % min(dfall[dfall['Iy/Ix'] < 0.5]['Percent Error K&T']))
    print('  + Minimum Percent Error using S16-14 Approximation: %.3f' % 
          min(dfall[dfall['Iy/Ix'] < 0.5]['Percent Error S16-14']))
    print('  + Maximum Percent Error using K&T Approximation: %.3f' % max(dfall[dfall['Iy/Ix'] < 0.5]['Percent Error K&T']))
    print('  + Maximum Percent Error using S16-14 Approximation: %.3f' %
          max(dfall[dfall['Iy/Ix'] < 0.5]['Percent Error S16-14']))
#+END_SRC

*** Plotting the % Error for All Standard WT-Shapes
    :PROPERTIES:
    :CUSTOM_ID: plotting-the-error-for-all-standard-wt-shapes
    :END:

-  Initialize and create Figure 4 in the technical note

#+BEGIN_SRC ipython :async
    set_style()
    fig4, ax4 = plt.subplots(nrows=1, ncols=2, sharey='row')
    mkr_dict_def = {1: 'Class 1', 2: 'Class 2', 3: 'Class 3', 4: 'Class 4'}
    mkr_dict = {1: '^', 2: 'o', 3: 'x', 4: '+'}
    color = {1: '#D3D3D3', 2: '#C0C0C0', 3: '#808080', 4: '#303030'}
    for kind in mkr_dict:
        dd1 = dfall[dfall['Section Class'] == kind]
        dd2 = dd1[dd1['Iy/Ix'] < 1.0]
        ax4[0].scatter(dd2['Iy/Ix'], dd2['Percent Error K&T'], color=color[kind], marker=mkr_dict[kind], label=mkr_dict_def[kind])
        ax4[0].tick_params(axis='both', which='major', labelsize=12)
        ax4[1].scatter(dd2['Iy/Ix'], dd2['Percent Error S16-14'], color=color[kind], marker=mkr_dict[kind], label=mkr_dict_def[kind])
        ax4[1].tick_params(axis='both', which='major', labelsize=12)

    # Set the y-axis label only for the left subplot
    ax4.flat[0].set_ylabel('Percent Error', fontsize='12')
    ax4.flat[0].set_xlabel(r'$\frac{I_y}{I_x}$', fontsize='18')
    ax4.flat[1].set_xlabel(r'$\frac{I_y}{I_x}$', fontsize='18')

    # Set x and y limits
    ax4.flat[0].set_ylim((-85,25))
    ax4.flat[0].set_xticks((0,0.2,0.4,0.6,0.8,1.0))
    ax4.flat[1].set_xticks((0,0.2,0.4,0.6,0.8,1.0))

    # Set the labels for each column
    ax4.flat[0].text(0.5,-0.45, "(a)", size=12, ha="center", transform=ax4.flat[0].transAxes)
    ax4.flat[1].text(0.5,-0.45, "(b)", size=12, ha="center", transform=ax4.flat[1].transAxes)

    # Create Legend
    plt.legend(loc='center left', bbox_to_anchor=(1.0, 0.5), fontsize='12')

    # Set size as per CJCE requirements
    set_size_onecol(fig4)

    # Set up tight plot layout
    plt.tight_layout()

    # Save Figure 4 as PNG and PDF and SVG
    plt.savefig('./paper_figs/Figure4.png', bbox_inches="tight")
    plt.savefig('./paper_figs/Figure4.pdf', bbox_inches="tight")
    plt.savefig('./paper_figs/Figure4.svg', bbox_inches="tight")
#+END_SRC
